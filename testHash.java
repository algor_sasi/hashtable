public class testHash {
    public static void main(String[] args) {
        Hash hash = new Hash(11);
        hash.put(1, "Ant");
        hash.put(15, "Bird");
        hash.put(8, "Cat");
        hash.put(4, "Dog");

        hash.getAll();
        System.out.println("--------------------------------");
        hash.put(12, "Elephant");
        hash.remove(1);
        hash.getAll();
    }
}
